import {Framework, Utils} from "floose";
import ComponentConfiguration = Framework.ComponentConfiguration;
import Driver = Framework.Driver;
import RequestMethod = Utils.HTTP.RequestMethod;
import Model = Framework.Data.Model;
import Repository = Framework.Data.Repository;
import SearchCriteria = Framework.Data.SearchCriteria;
import Join = Framework.Data.Join;
import Schema = Utils.Validation.Schema;

export interface ODataDriverConfig extends ComponentConfiguration {
    version: string;
    baseUri: string;
    headers?: {
        [header: string]: string;
    };
}

export declare class ODataDriver extends Driver {
    readonly configurationValidationSchema: Schema;
    init(config: ODataDriverConfig): Promise<void>;
    setCustomHeaders(customHeaders: {
        [index: string]: string;
    }): ODataDriver;
    request(method: RequestMethod, headers: {
        [index: string]: string;
    }, basePath: string, qs?: {
        [index: string]: string;
    }, body?: any): Promise<{
        statusCode: number;
        statusMessage: string;
        body: any;
    }>;
    find(basePath: string, qs?: {
        [index: string]: string;
    }): Promise<{
        [index: string]: any;
    }[]>;
    get(basePath: string, identifier: string): Promise<{
        [index: string]: any;
    }>;
    post(basePath: string, object: {
        [index: string]: any;
    }): Promise<{
        [index: string]: any;
    }>;
    put(basePath: string, identifier: string, object: {
        [index: string]: any;
    }, eTag?: string): Promise<void>;
    patch(basePath: string, identifier: string, object: {
        [index: string]: any;
    }, eTag?: string): Promise<void>;
    delete(basePath: string, identifier: string, eTag?: string): Promise<void>;
}

export declare abstract class AbstractODataModel extends Model {
}

export declare abstract class AbstractODataRepository<T extends AbstractODataModel> extends Repository<T> {
    protected _connection: string;
    constructor(connection: string);

    protected abstract _collectionName(): string;
    protected abstract _collectionIdentifier(): string;
    protected abstract _eventPrefix(): string;
    protected abstract _modelClass(): {new(): T};

    protected _find(searchCriteria: SearchCriteria, joinCriteria?: Join[]): Promise<T[]>;

    create(): T;
    get(identifier: any, forceReload?: boolean): Promise<T>;
    save(modelInterface: T): Promise<T>;
    delete(modelInterface: T | T[]): Promise<void>;
}